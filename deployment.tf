resource "kubernetes_deployment" "echoserver-deploy" {
  metadata {
    name      = "echoserver-deploy"
    namespace = kubernetes_namespace.echoserver-namespace.metadata.0.name
    labels = {
      app = "echoserver"
    }
  }

  lifecycle {
    ignore_changes = [
      metadata[0].annotations,
      metadata[0].labels,
    ]
  }

  spec {
    selector {
      match_labels = {
        app = "echoserver"
      }
    }

    strategy {
      type = "RollingUpdate"
    }

    template {
      metadata {
        labels = {
          app = "echoserver"
        }
      }

      spec {
        automount_service_account_token = "true"
        container {
          name              = "echoserver"
          image             = var.echoserver_image
          image_pull_policy = "Always"

          port {
            container_port = "8080"
            name           = "http"
          }

        }
      }
    }
  }
  timeouts {
    create = "5m"
    update = "5m"
    delete = "2m"
  }
  depends_on = [
    kubernetes_namespace.echoserver-namespace,
  ]
}
resource "kubernetes_namespace" "echoserver-namespace" {
  metadata {
    name = "echoserver-namespace"
  }

  lifecycle {
    ignore_changes = [
      metadata[0].annotations,
      metadata[0].labels,
    ]
  }
}

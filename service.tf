resource "kubernetes_service_v1" "echoserver-service" {
  metadata {
    name      = "echoserver-service"
    namespace = kubernetes_namespace.echoserver-namespace.metadata.0.name
    labels = {
      app = "echoserver"
    }
  }

  lifecycle {
    ignore_changes = [
      metadata[0].annotations,
      metadata[0].labels,
    ]
  }

  spec {
    type = "LoadBalancer"

    selector = {
      app = "echoserver"
    }

    port {
      port        = "80"
      target_port = "8080"
      name        = "http"
      protocol    = "TCP"
    }
  }
  depends_on = [
    kubernetes_namespace.echoserver-namespace,
  ]
}

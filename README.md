# ECHO

## Purpose

Simple aplication deployed 100% using Terraform.

This application is usefull to test conectivity with the cluster.

This project consists basically of:
- A deployment: deploys a echoserver container
- A service: exposes the container
- A namespace: houses the resources above

## Dependencies

The CI of this project depends on the following environment variables that should be defined in GitLab:

| Name                    | Value                                     |
|-------------------------|-------------------------------------------|
| AWS_ACCESS_KEY_ID       | access key to aws account                 |
| AWS_SECRET_ACCESS_KEY   | secret key to aws account                 |
| S3_BUCKET               | name of the bucket on aws (for tfstate)   |
| S3_REGION               | region of the bucket on aws (for tfstate) |